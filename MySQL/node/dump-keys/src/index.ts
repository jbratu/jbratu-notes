//
// Dump keys to a file
//
// Launch using:
// npx tsx src/index.ts
//

import * as mysql from 'mysql2/promise';
import * as dotenv from 'dotenv';
import * as fs from 'fs/promises';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

dotenv.config();

interface Arguments {
    database: string;
    table: string;
    key: string;
    output: string;
    chunkSize?: number;
}

const host = process.env['DB_HOST'] ?? '';
const user = process.env['DB_USER'] ?? '';
console.info(`Connecting to ${host} as ${user}`);

// Parse command line arguments
const argv = yargs(hideBin(process.argv))
    .option('database', {
        alias: 'd',
        type: 'string',
        description: 'Database name',
        demandOption: true
    })
    .option('table', { alias: 't', type: 'string', description: 'Table name', demandOption: true })
    .option('key', { alias: 'k', type: 'string', description: 'Key column name', demandOption: true })
    .option('output', { alias: 'o', type: 'string', description: 'Output file path', demandOption: true })
    .option('chunkSize', { 
        alias: 'c', 
        type: 'number', 
        description: 'Number of records to process per chunk',
        default: 5000 
    })
    .help()
    .argv as Arguments;

const mpiCmd = async function(connection: mysql.Connection) {
    const DATABASE_NAME = argv.database;
    const TABLE_NAME = argv.table;
    const KEY_CHUNK_COUNT = argv.chunkSize || 5000;
    const KEY_NAME = argv.key;
    const outputFile = argv.output;

    try {
        // Get total count of records
        const [countResult] = await connection.query(
            `SELECT COUNT(*) as total FROM ${DATABASE_NAME}.${TABLE_NAME}`
        ) as any[];
        const totalRecords = countResult[0].total;
        const totalPages = Math.ceil(totalRecords / KEY_CHUNK_COUNT);
        console.log(`Found ${totalRecords} total records. Processing in ${totalPages} pages...`);

        // Create output file
        await fs.writeFile(outputFile, ''); // Initialize empty file

        // Process records in chunks
        for (let page = 0; page < totalPages; page++) {
            const offset = page * KEY_CHUNK_COUNT;
            
            const [records] = await connection.query(
                `SELECT ${KEY_NAME} FROM ${DATABASE_NAME}.${TABLE_NAME} 
                 LIMIT ? OFFSET ?`,
                [KEY_CHUNK_COUNT, offset]
            ) as any[];

            // Format and write chunk to file
            const chunk = records.map((record: any) => 
                `${record.id}`
            ).join('\n');

            await fs.appendFile(outputFile, chunk + '\n');
            
            console.log(`Processed page ${page + 1}/${totalPages}`);
        }

        console.log(`Complete! Keys written to ${outputFile}`);
    } catch (error) {
        console.error('Error processing keys:', error);
        throw error;
    }

    // End the routine
    process.exit();
}

// Create the connection outside of mpiCmd
const createConnection = async () => {
    return await mysql.createConnection({
        host: host,
        user: user,
        password: process.env['DB_PASSWORD'] as string,
        database: process.env['DB_DATABASE'] as string,
        port: process.env['DB_PORT'] as unknown as number
    });
};


createConnection()
    .then(connection => mpiCmd(connection))
    .then(
        (response) => console.log(response),
        (error) => console.error(error)
    )
    .finally(() => {});
