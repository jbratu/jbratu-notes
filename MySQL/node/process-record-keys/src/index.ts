//
// Given a list of keys and table information perform the action in template.sql on the list of keys
//
// Launch using:
// npx tsx src/index.ts --database thedb --table debug_log_objects --key id --keyFile "c:\temp\test.tst"
//

import * as mysql from 'mysql2/promise';
import * as dotenv from 'dotenv';
import * as fs from 'fs/promises';
import * as fsSync from 'fs';
import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';
import * as path from 'path';
import * as readline from 'readline';

dotenv.config();

interface Arguments {
    database: string;
    table: string;
    keyFile: string;
}

const host = process.env['DB_HOST'] ?? '';
const user = process.env['DB_USER'] ?? '';
console.info(`Connecting to ${host} as ${user}`);

// Parse command line arguments
const argv = yargs(hideBin(process.argv))
    .option('database', {
        alias: 'd',
        type: 'string',
        description: 'Database name',
        demandOption: true
    })
    .option('table', { alias: 't', type: 'string', description: 'Table name', demandOption: true })
    .option('keyFile', { alias: 'o', type: 'string', description: 'File containing the list of keys to process', demandOption: true })
    .help()
    .argv as Arguments;

const mpiCmd = async function(connection: mysql.Connection) {
    const DATABASE_NAME = argv.database;
    const TABLE_NAME = argv.table;
    const inputFile = argv.keyFile;
    const processedKeysFile = path.join(path.dirname(inputFile), 'processedKeys');
    const templateFile = path.join(__dirname, '../template.sql');
    
    // Read SQL template
    const sqlTemplate = await fs.readFile(templateFile, 'utf8');
    
    // Create Set of processed keys
    const processedKeys = new Set<string>();
    try {
        const processed = await fs.readFile(processedKeysFile, 'utf8');
        processed.split('\n').forEach(key => processedKeys.add(key.trim()));
    } catch (err) {
        // File doesn't exist yet, which is fine
    }
    
    // Create read stream for input file
    const fileStream = fsSync.createReadStream(inputFile, {
        encoding: 'utf8',
        highWaterMark: 64 * 1024 // 64KB chunks
    });
    
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });
    
    // Process each line (key)
    for await (const key of rl) {
        const trimmedKey = key.trim();
        if (!trimmedKey || processedKeys.has(trimmedKey)) {
            continue;
        }

        try {
            // Replace {KEY} in template with actual key
            const sql = sqlTemplate.replace('{KEY}', trimmedKey);
            
            // Execute SQL
            await connection.query(sql);
            
            // Append to processed keys file
            await fs.appendFile(processedKeysFile, trimmedKey + '\n');
            processedKeys.add(trimmedKey);
            
            console.log(`Processed key: ${trimmedKey}`);
        } catch (err) {
            console.error(`Error processing key ${trimmedKey}:`, err);
        }
    }
    
    console.log('Processing complete');
    await connection.end();

    // End the routine
    process.exit();
}

// Create the connection outside of mpiCmd
const createConnection = async () => {
    return await mysql.createConnection({
        host: host,
        user: user,
        password: process.env['DB_PASSWORD'] as string,
        database: process.env['DB_DATABASE'] as string,
        port: process.env['DB_PORT'] as unknown as number
    });
};

createConnection()
    .then(connection => mpiCmd(connection))
    .then(
        (response) => console.log(response),
        (error) => console.error(error)
    )
    .finally(() => {});
