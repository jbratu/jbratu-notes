SELECT date_format(dt, '%Y-%m-%d') as CheckDate, round(AVG(duration),0) as AvgDur
FROM `mpi_performance_metrics`
WHERE `service` = 'tenants_call_oi.listTenants'
AND dt > '2019-02-01'
AND lid = 192
Group By date_format(dt, '%Y-%m-%d')
LIMIT 50