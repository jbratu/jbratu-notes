#First and last watchdog entry
select wid, from_unixtime(min(timestamp)), from_unixtime(max(timestamp)) from watchdog order by wid desc;

SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME IN ('columnA','ColumnB') AND TABLE_SCHEMA='YourDatabase';

describe watchdog;

select * from watchdog where message like "New tenant record created%" and `variables` like "%4257%" limit 100;
select from_unixtime(timestamp) as ts, watchdog.* from watchdog where (message like "Tenant record updated%" OR message like "New tenant record created%") and `variables` like "%4262%" limit 100;

select * 


#List the watchdog earliest event and the latest event
select (select from_unixtime(timestamp) from watchdog order by `timestamp` asc limit 1) as log_start, (select from_unixtime(timestamp) from watchdog order by `timestamp` desc limit 1) as log_end from watchdog limit 1;

#Search watchdog log for message or variable matching text
SET @SEARCH='%CAAAN8GNR%';
select from_unixtime(timestamp) as ts,users.name, watchdog.* from watchdog
left join users on users.uid = watchdog.uid
 where message like @SEARCH OR `variables` like @SEARCH order by wid desc limit 100;

#Search watchdog log for message or variable matching text after a specific time
SET @SEARCH='%5142%';
SET @DATETIME_FROM = '2018-10-01 00:00:00';
select from_unixtime(timestamp) as ts,users.name, watchdog.* from watchdog
left join users on users.uid = watchdog.uid
 where message like @SEARCH OR `variables` like @SEARCH and
from_unixtime(timestamp) > @DATETIME_FROM limit 100;

#Search watchdog log for specific variable
SET @SEARCH='a:3:{s:4:"!lid";s:2:"86";s:7:"!tenant";s:0:"";s:8:"!account";s:0:"";}';
select from_unixtime(timestamp) as ts, watchdog.* from watchdog where `variables` = @SEARCH limit 100;

#Search watchdog log for message from a specific user
SET @SEARCH='%login%';
select from_unixtime(timestamp) as ts, watchdog.wid, users.name, watchdog.message, watchdog.variables 
from watchdog left join users on users.uid = watchdog.uid where users.name in('CFHbiller', 'CFHdental', 'CFHpost') and (message like @SEARCH OR `variables` like @SEARCH) limit 100;

#Search log messages after specific time but exclude certain users
SET @DATETIME_FROM = '2018-10-01 00:00:00';
select from_unixtime(timestamp) as ts, watchdog.wid, users.name, watchdog.message, watchdog.variables from watchdog
left join users on users.uid = watchdog.uid where users.name not in('cronaccount','') and
from_unixtime(timestamp) > @DATETIME_FROM limit 100;

#Search log messages after specific time but INCLUDE certain users
SET @DATETIME_FROM = '2018-10-01 00:00:00';
select from_unixtime(timestamp) as ts, watchdog.wid, users.name, watchdog.message, watchdog.variables from watchdog
left join users on users.uid = watchdog.uid where users.name in('cronaccount','') and
from_unixtime(timestamp) > @DATETIME_FROM limit 100;

#Search log messages after specific time for specific user
SET @DATETIME_FROM = '2015-05-18 00:00:00';
select from_unixtime(timestamp) as ts, watchdog.wid, users.name, watchdog.message, watchdog.variables from watchdog
left join users on users.uid = watchdog.uid where users.name in('CFHbiller', 'CFHdental', 'CFHpost') and
from_unixtime(timestamp) > @DATETIME_FROM;

#Search log messages after specific time for specific user
#Including certain text
SET @SEARCH='%logged into backend database location%';
SET @DATETIME_FROM = '2021-10-18 00:00:00';
SET @DATETIME_TO = '2021-10-19 00:00:00';
select from_unixtime(timestamp) as ts, watchdog.wid, users.name, watchdog.message, watchdog.variables from watchdog
left join users on users.uid = watchdog.uid where users.name in('avillanueva') 
and from_unixtime(timestamp) > @DATETIME_FROM
and from_unixtime(timestamp) < @DATETIME_TO
and message like @SEARCH OR `variables` like @SEARCH order by wid desc limit 100;


#Select log messages after specific time
SET @DATETIME_FROM = '2020-03-02 15:41:16';
select from_unixtime(timestamp) as ts, watchdog.wid, users.name, watchdog.message, watchdog.variables from watchdog
left join users on users.uid = watchdog.uid and
from_unixtime(timestamp) > @DATETIME_FROM limit 100;

#Search watchdog log for message or variable matching text
#After specific date time
#Only specific log type
SET @SEARCH='%CEICHEN01%';
SET @LOGTYPE='practice';
SET @DATETIME_FROM = '2014-05-21 00:00:00';
select from_unixtime(timestamp) as ts, watchdog.* from watchdog where 
type = @LOGTYPE and 
from_unixtime(timestamp) > @DATETIME_FROM AND 
(message like @SEARCH OR `variables` like @SEARCH)
 limit 100;

#
#After specific date time
#Only specific log type
#
SET @DATETIME_FROM = '2019-02-06 08:00:00';
select from_unixtime(timestamp) as ts, watchdog.* from watchdog where 
type in('f_ch_g_ecl', 'claimhistory') and 
from_unixtime(timestamp) > @DATETIME_FROM
order by wid asc
 limit 100;

#
#After specific date time
#
SET @DATETIME_FROM = '2019-08-28 14:00:00';
select from_unixtime(timestamp) as ts, watchdog.* from watchdog where 
from_unixtime(timestamp) > @DATETIME_FROM
order by wid asc
 limit 100;


#search by time range for logs of specific type
SET @SEARCH_FROM = '2014-05-14 21:00:00';
SET @SEARCH_TO = '2014-05-16 00:00:00';
select from_unixtime(timestamp) as ts, watchdog.* from watchdog where from_unixtime(timestamp) > @SEARCH_FROM and from_unixtime(timestamp) < @SEARCH_TO and
type in('claims') 
order by timestamp asc limit 2000;


#Select watchdog logs in specific time range
#Select logs not of a specific type
#Select specific log entries
#Order from oldest to most recent
select from_unixtime(timestamp) as ts, watchdog.* from watchdog where from_unixtime(timestamp) > '2014-04-01 21:00:00' and from_unixtime(timestamp) < '2014-04-02 00:00:00' and
type not in('cron','CPE Notify', 'page not found', 'access denied', 'catbuild_upcache', 'COUPON BASED', 'oi_cart_debug', 'uc_cart', 'search')  and
wid in (3425451, 3425554,3426601) 
order by timestamp asc limit 2000;

#Show watchdog logs before and after specified ID
SET @TARGET_WID = 21480270;
SET @WID_RANGE = 50;
select from_unixtime(timestamp) as ts, watchdog.* from watchdog where watchdog.wid > (@TARGET_WID - @WID_RANGE) AND watchdog.wid < (@TARGET_WID + @WID_RANGE);
