SELECT (t1.wid + 1) as gap_starts_at,
       (SELECT MIN(t3.wid) -1 FROM watchdog t3 WHERE t3.wid > t1.wid) as gap_ends_at
FROM watchdog t1
WHERE NOT EXISTS (SELECT t2.wid FROM watchdog t2 WHERE t2.wid = t1.wid + 1)
HAVING gap_ends_at IS NOT NULL;