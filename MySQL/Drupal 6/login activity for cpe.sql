#Find all users by login activity except from within the office
select distinct hostname from watchdog where message = 'Session opened for %name.' and hostname not in ('96.64.98.169') and variables REGEXP 'cpe_admin|joesagg|rsalvi|alotary|bjohnson|pnerozzi|cpetest_contenteditor|slevy|1digitalagency|jasonwood|phoogt|joesagg9|bmichael|cpeinc|mmarkar|anubhav|oecgi' limit 100;

#Report on the user login activity
select wid, from_unixtime(timestamp) as ts, message, variables, hostname from watchdog where message = 'Session opened for %name.' and hostname not in ('96.64.98.169') and variables REGEXP 'cpe_admin|joesagg|rsalvi|alotary|bjohnson|pnerozzi|cpetest_contenteditor|slevy|1digitalagency|jasonwood|phoogt|joesagg9|bmichael|cpeinc|mmarkar|anubhav|oecgi' order by wid;
