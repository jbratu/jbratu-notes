REM Copy everything
robocopy Z:\Revsoft\OpenInsight\ "\\remoteserver\Share\OpenInsight" * /MIR /NP /R:1 /W:1 /LOG+:"copylog.txt"

REM Replace the copied revparam files with those from the destination server
robocopy "\\remoteserver\C$\Revsoft\Universal Driver 5.1" "\\remoteserver\Share\OpenInsight" revparam

SET ERRORLEVEL=0