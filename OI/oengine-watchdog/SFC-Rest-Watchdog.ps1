﻿#
# The purpose of this script is to:
# Start the OEngine server service in debug mode
# Check the REST API running in the OEngine server service
# Restart the OEngine server if a continuous threshold of failed checks are reached
#

#
# Configuration is read from a JSON file. See example-config.json
#

$ThisScriptName = [io.path]::GetFileNameWithoutExtension($MyInvocation.MyCommand.Name)
$ConfigFile = [environment]::getfolderpath("mydocuments") + "\$ThisScriptName-config.json"

Write-Host "Reading configmration from $ConfigFile"

try {
    $Config = Get-Content $ConfigFile | ConvertFrom-Json 
} catch {
    Write-Host "Configuration file is not valid";
    Return;
}

#How many times can the check fail before the process is considered dead?
$max_failed_checks = $config.max_Failed_checks;

#How long between health checks in seconds?
$check_interval = $config.check_interval;

#When failed, the process will take this long to respawn:
#$check_interval * max_failed_checks * $interval_duration (assuming 1 second)

#What process should be started and watched?
$watched_exe_dir = $config.watched_exe_dir;
$watched_exe = $config.watched_exe;
$watched_exe_args = $config.watched_exe_args;
$watched_port = $config.watched_port;

#What to watch
$url = $config.url;
$healthy_indicator = $config.healthy_indicator;

#Where to log
$LogFile = "$($env:TEMP)\$ThisScriptName.log";
Start-Transcript -path $LogFile -Append
Write-Host "Logging to $LogFile"

#
#Internal variables
#

#How many times has the check failed
$failed_check_counter = 0;
#How long should the script sleep between attempts to poll for quit keystroke
$interval_duration = 1
#How many poll loops have passed
$interval_counter = 0;

#How many times has the process been restarted
$restart_count = 0

#Start the first copy of the watched process
Set-Location $watched_exe_dir;
$watched_pid = Start-Process $watched_exe $watched_exe_args -passthru
Write-Host "Starting process $($watched_pid.id)";

$done = $false;
Do {

    Write-Host "$((Get-Date).ToString()) Press q to stop watching. Restarted process $restart_count time(s). Running health check now...";

    #Assume the health check executed
    $check_executed = $true;

    try {
        $health_check_content = Invoke-WebRequest -Uri $url -UseBasicParsing
    } catch {
        #Check didn't execute, perhaps due to a local network failure. State of the check is indeterminate.
        $check_executed = $false;
        Write-Host "Unable to query REST API.";
    }

    if ($check_executed -and ($health_check_content -like "*$healthy_indicator*")) {
        #Check passed so reset the failed counter
        $failed_check_counter = 0;
    } else {
        #Check failed so start counting how many times it's failed
        $failed_check_counter += 1;
        Write-Host "The check interval failed $failed_check_counter time(s). Restarting after $max_failed_checks failed checks.";
        Write-Host "The healthy indicator string $healthy_indicator was not found in the response: `r`n $health_check_content";
    }


    if ($failed_check_counter -gt $max_failed_checks) {
        Write-Host "Check limit reached, stopping process $($watched_pid.id)";
        Stop-Process -id $watched_pid.id -Force

        #Get list of all OEngine processes still running
        $OEProcesses = get-process -Name "OENGINE" -ErrorAction SilentlyContinue
        for ($i = 0; $i -lt $OEProcesses.count; $i++) {
            
            #Get one process and see if it was launched from our watched DIR
            $Process = $OEProcesses[$i];

            if ($Process.path -like "$watched_exe_dir\*") {
                #This process is running from the same directory. Was we double checking the port before killing engines?

                $ok_to_kill = $false;

                if ($watched_port -ne "") {
                    #Get the command line arguments for the OENGINE process
                    $args = Get-CimInstance Win32_Process -Filter "ProcessID = $($Process.id)" | select-object CommandLine

                    if($args -like "*S=L_$watched_port_*") {
                        #Matched both the listening port based on cmd line arguments and start-up dir so we can kill it.
                        $ok_to_kill = $true;
                    } else {
                        #Although it started from the right directory it's listening on a different port so it must be for a different process.
                        #Don't kill this one.
                    }

                } else {
                    #We don't care what port, it's running from the right DIR so kill it.
                    $ok_to_kill = $true;
                }

                if($ok_to_kill -eq $true) {
                    #Yes, it is from the watched system and still running so kill it.
                    Write-Host "Killing oengine processes $($Process.id)";
                    Stop-Process -id $Process.id -Force
                }
                
            }
        }
        
        #Now that the OESocketServer and child processes have been terminated start a new engine server.
        Set-Location $watched_exe_dir;
        $watched_pid = Start-Process $watched_exe $watched_exe_args -passthru
        Write-Host "Starting new process $($watched_pid.id)";

        $restart_count += 1;

        #Restart our checks
        $failed_check_counter = 0;

    }

    #Sleep for an interval but watch for keypress to control script
    Do {

        if ([console]::KeyAvailable)
            {

            $x = [System.Console]::ReadKey() 
            switch ( $x.key)
            {
                q { $done = $true }
            }
        }

        #Delay before next check
        sleep $interval_duration;

        $interval_counter += 1;
    } Until (($interval_counter -gt $check_interval) -or ($done -eq $true))

    #Reset for next check
    $interval_counter = 0

} Until ($done -eq $true)

Write-Host "Watchdog process ended.";
Stop-Process -id $watched_pid.id -Force

Stop-Transcript