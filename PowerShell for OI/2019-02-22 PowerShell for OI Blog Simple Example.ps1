#Path to the .NET DLL containing NetOI installed by OpenInsight's clientsetup.exe
$DLLPath = "C:\Program Files (x86)\Revelation Software\Revelation Software .NET 4.0 Components v9.4.0\RevelationDotNet4.dll";

#Connection information for your script to access the OEngine service.
$OEHost = "localhost";
$Port = 8088;
$OIApp = "EXAMPLES";
$OIUser = "EXAMPLES";
$OIPass = "";

#Create a connection object
$Server = New-Object "Revelation4.NetOI.Server"

#Connect to the server
$Server.OIConnect($OEHost, $Port, $OIApp, $OIUser, $OIPass);

#According to the OI help file SELECT_INTO has two parameters.
#We will pass those parameters as an array.
$Args = @(
    'LIST CUSTOMERS FNAME LNAME PHONE',
    'TAB'
);

#Use the connection to execute SELECT_INTO with our parameters.
$Result = $Server.CallFunction('SELECT_INTO',[ref] $Args);

#Display the results and disconnect
Write-Host $Result;

$Server.OIDisconnect();