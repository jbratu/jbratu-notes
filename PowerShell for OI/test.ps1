$DLLPath = "C:\Program Files (x86)\Revelation Software\Revelation Software .NET 4.0 Components v9.4.0\RevelationDotNet4.dll";
#$DLLPath = "C:\Program Files (x86)\Revelation Software\Revelation Software .NET Components v9.5\RevelationDotNet.dll"
#$DLLPath = "C:\Revsoft\oi943\netoiexampleform\netoiexampleform\bin\Debug\NetOI.dll"

$OITypes = Add-Type -Path $DLLPath

$asm = [System.Reflection.Assembly]::LoadFrom($dllpath)
$cbbtypes = $asm.GetExportedTypes()
#$cbbtypes | Get-Member -Static

$Server = New-Object "Revelation4.NetOI.Server"

$OEHost = "localhost";
$Port = 8088;
$OIApp = "EXAMPLES";
$OIUser = "EXAMPLES";
$OIPass = "";

$Server.PersistentConnection = $true;
$Server.OIConnect($OEHost, $Port, $OIApp, $OIUser, $OIPass);

Write-Host "----------- read write test ------";

$TableName = "CUSTOMERS"
$Table = $Server.OpenFile($TableName)
$TestKey = "1"
$Rec = $Table.Read($TestKey) 
#Write-Host $Rec.Record

$Keys = $Server.SelectFile($Table);

ForEach ($Key in $Keys) {
#Write-Host "Table $TableName has key $Key";
}

$Rec.Field(1,0,0) = "PowerShell"
$Rec.Field(2,0,0) = "Test"

$Server.WriteRecord($Table, $TestKey, $Rec)

$Rec = $Table.Read($TestKey) 
#Write-Host $Rec.Record

#$SelectedKeys = $Server.ExecuteSelect("SELECT 10 CUSTOMERS");
#ForEach ($Key in $SelectedKeys) {
#    Write-Host "Found table $TableName key $Key";
#}


#$Fields = @('ID','FNAME', 'LNAME', 'PHONE');

Write-Host "----------- select test ------";

#$Args = @('LIST CUSTOMERS FNAME LNAME PHONE','TAB');
#$Result = $Server.CallFunction('SELECT_INTO',[ref] $Args);

$Q = @(
'SELECT CUSTOMERS WITH PHONE NE ""',
'SELECT CUSTOMERS WITH LNAME STARTING WITH "C"',
'LIST CUSTOMERS FNAME LNAME PHONE'
);

$Qs = $Q -join ';';

$Args = @($Qs,'TAB');
$Result = $Server.CallFunction('SELECT_INTO',[ref] $Args);


Write-Host "----------- table prep ------";

$Lines = $Result -split "\n";
$Table = @();

$LineNum = 0;
ForEach ($Line in $Lines) {
    $Line = $Line -split "\t";

    $Line = $Line | foreach{ $_.Trim()};

    $O = New-Object PsCustomObject;

    If ($LineNum -eq 0) {
   
        $Fields = $Line;
       
    } else {

        for($i = 0; $i -lt $Line.Count;$i++) {
           $O | Add-Member NoteProperty $Fields[$i] $Line[$i];
        }

        $Table += $o;

    }

    $LineNum += 1;
}

$Server.OIDisconnect();


RETURN

$Args = @('SELECT CUSTOMERS WITH PHONE NE ""',5,'','','');
$Server.CallsUB('RLIST',[ref] $Args);

$Args = @('SELECT CUSTOMERS WITH LNAME STARTING WITH "C"',5,'','','');
$Server.CallsUB('RLIST',[ref] $Args);

$Args = @('LIST CUSTOMERS FNAME LNAME PHONE','TAB');
$Result = $Server.CallFunction('SELECT_INTO',[ref] $Args);