﻿#Where are the logs that need to be organized?
$LogPathSource = "C:\httplogs";

#What files in $LogPathSource are considered to be log files?
$LogExtension = "*.log";

#Where should the logs be stored after organization?
$LogPathDest = "c:\httplogs\organized";

#Check the organized folder exists
New-Item -Path $LogPathDest -ItemType "directory" -Force | Out-Null;

#Get all the files to be organized
$LogFiles = Get-ChildItem -Path $LogPathSource -Filter $LogExtension;

#Store a hash table of dates from file names
$LogDates = @{};

#Loop through all the files and organize them by date
ForEach($LogFile in $LogFiles) {

    $FileName = $LogFile.Name;

	#Doesn't work on Server 2008
    #$FileNameDate = [regex]::Match($FileName,'^(.*?)_').captures.groups[1].value;
	
	#Works in Server 2008
	$FileNameDate = $FileName.Split('_')[0];
    
	#Have we seen this date before?
    if ($LogDates[$FileNameDate] -eq $null) {
		#No, so create an array list and store it in the hash table under the date key
        Write-Host "Found logs for new date: $FileNameDate";
        [System.Collections.ArrayList]$NewArray = @($FileName);
        $LogDates[$FileNameDate] = 	$NewArray;
    } else {
		# Yes, so add it to the array of filenames for that date
        # Write-Host "Adding $FileNameDate";
        $LogDates[$FileNameDate].Add($FileName) | Out-Null;
    }

}

#Loop through the hashtable of all the dates seen
#and process the arraylist of files for that date
ForEach($LogDate In $LogDates.keys) {

    Write-Host "Processing log dates $LogDate";

    $LogFolderForDate = $LogPathDest + "\" + $LogDate;

	#Make sure the organization folder exists
    New-Item -Path $LogFolderForDate -ItemType "directory" -Force | Out-Null;

    $LogFilesForDate = $LogDates[$LogDate];

	#Loop through all the files for that date
    ForEach ($LogFileName in $LogFilesForDate) {

        $Src = $LogPathSource + "\" + $LogFileName;
        $Dst = $LogFolderForDate + "\" + $LogFileName;

        Write-Host "Moving $Src to $Dst";
        Move-Item -Path $Src -Destination $Dst -Force;

    }

}
