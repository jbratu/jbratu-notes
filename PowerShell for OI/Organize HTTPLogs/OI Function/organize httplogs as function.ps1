﻿
function Archive-SRPHTTPLogs {
param(
    [string]$OlderThanDateStr = "",
    [string]$LogPathSource = "C:\httplogs",
    [string]$LogPathDest = "c:\httplogs\archive"
)
$progresspreference="SilentlyContinue";

#Uncomment this to simulate an error
#Blah $Failed;

if($OlderThanDateStr -ne "") {
    #Convert the supplied date to an object
    #Throws error if not a valid date

    try {
        $OlderThanDate = [datetime]$OlderThanDateStr;
    } catch {
        throw $_;
        return;
    }
}

#What files in $LogPathSource are considered to be log files?
$LogExtension = "*.log";

#Check the organized folder exists
New-Item -Path $LogPathDest -ItemType "directory" -Force | Out-Null;

#Get all the files to be organized
$LogFiles = Get-ChildItem -Path $LogPathSource -Filter $LogExtension;

#Store a hash table of dates from file names
$LogDates = @{};

#Loop through all the files and organize them by date
ForEach($LogFile in $LogFiles) {

    $FileName = $LogFile.Name;

	#Doesn't work on Server 2008
    #$FileNameDate = [regex]::Match($FileName,'^(.*?)_').captures.groups[1].value;
	
	#Works in Server 2008
	$FileNameDate = $FileName.Split('_')[0];

    $FileNameDateObj = $Null;

    try {
        $FileNameDateObj = [datetime]$FileNameDate;
    } catch {
        #This was not a valid date so skip
    }

    if ($FileNameDateObj -ne $Null) {
        #This filename has a valid date

        #Is it within our range?
        if($FileNameDateObj -lt $OlderThanDate) {
            #Yes, the file name is old enough to archive

	        #Have we seen this date before?
            if ($LogDates[$FileNameDate] -eq $null) {
		        #No, so create an array list and store it in the hash table under the date key
                #Write-Host "Found logs for new date: $FileNameDate";
                [System.Collections.ArrayList]$NewArray = @($FileName);
                $LogDates[$FileNameDate] = 	$NewArray;
            } else {
		        # Yes, so add it to the array of filenames for that date
                # Write-Host "Adding $FileNameDate";
                $LogDates[$FileNameDate].Add($FileName) | Out-Null;
            }

        } else {
            #No, the file name is newer than our cut-off date
        }

    } else {
        #The file name did not have a valid date entry in the filename so skip
    }

}

#Loop through the hashtable of all the dates seen
#and process the arraylist of files for that date
ForEach($LogDate In $LogDates.keys) {

    #Write-Host "Processing log dates $LogDate";

    $LogFolderForDate = $LogPathDest + "\" + $LogDate;

	#Make sure the organization folder exists
    New-Item -Path $LogFolderForDate -ItemType "directory" -Force | Out-Null;

    $LogFilesForDate = $LogDates[$LogDate];

	#Loop through all the files for that date
    ForEach ($LogFileName in $LogFilesForDate) {

        $Src = $LogPathSource + "\" + $LogFileName;
        $Dst = $LogFolderForDate + "\" + $LogFileName;

        #Write-Host "Moving $Src to $Dst";
        Move-Item -Path $Src -Destination $Dst -Force;

    }

}

#Loop through the hashtable of all the dates seen
#and compress the files that were organized into folders
ForEach($LogDate In $LogDates.keys) {

    #Figure out which archive date we want to compress
    $LogFolderForDate = $LogPathDest + "\" + $LogDate;

    #The name of the zip file where the archived date should reside
    $LogFolderZipFile = $LogPathDest + "\" + $LogDate + ".zip"

    try {
        #Compress it, fail if any error
        Compress-Archive -Path $($LogFolderForDate + "\" + $LogExtension) -DestinationPath $LogFolderZipFile -ErrorAction Stop

        #Remove the source log files
        Remove-Item -Path $LogFolderForDate -Recurse -Force
    } catch {
        throw $_;
        return
    }
    
}



} #End Archive-SRPHTTPLogs

try {
    Archive-SRPHTTPLogs -OlderThanDate '{{ArchiveOlderThanDate}}' -LogPathSource '{{ArchiveLogsFromFolder}}' -LogPathDest '{{ArchiveStorageFolder}}';
    Return "OK";
} catch {
    Return "ERROR`n" + $_.ToString() ;
}