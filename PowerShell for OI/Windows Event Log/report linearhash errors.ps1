# Must be used with PowerShell 7
# This script does the following:
# 1. Prompts for start date, end date (defaults to tomorrow if blank), and source name to search.
# 2. Searches the Application event log for events within the date range and matching the source name.
# 3. Extracts filenames from events containing "File " in their data.
# 4. Returns a sorted list of unique filenames.
# Usage: Copy and paste the entire line below into a PowerShell prompt and press Enter.


$startDate = Read-Host "Enter start date (MM/DD/YY)"; $endDate = Read-Host "Enter end date (MM/DD/YY) or press Enter for tomorrow's date"; $endDate = if ([string]::IsNullOrWhiteSpace($endDate)) { (Get-Date).AddDays(1).ToString("MM/dd/yy") } else { $endDate }; $searchString = Read-Host "Enter source name to search"; Get-WinEvent -FilterHashtable @{LogName='Application'; StartTime=(Get-Date $startDate); EndTime=(Get-Date $endDate); ProviderName="*$searchString*"} | ForEach-Object { ([xml]$_.ToXml()).Event.EventData.Data | Where-Object { $_ -like "File *" } } | ForEach-Object { ($_ -split "File ", 2)[1].Trim() } | Select-Object -Unique | Sort-Object