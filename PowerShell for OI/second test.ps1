﻿#Path to the .NET DLL containing NetOI installed by OpenInsight's clientsetup.exe
$DLLPath = "C:\Program Files (x86)\Revelation Software\Revelation Software .NET 4.0 Components v9.4.0\RevelationDotNet4.dll";

#Connection information for your script to access the OEngine service.
$OEHost = "localhost";
$OEPort = 8088;
$OIApp = "EXAMPLES";
$OIUser = "EXAMPLES";
$OIPass = "";

#Create a connection object
$Server = New-Object "Revelation4.NetOI.Server"

#Connect to the server
$Server.OIConnect($OEHost, $OEPort, $OIApp, $OIUser, $OIPass);

#The SELECT_INTO function accepts a semicolon separated
#list of SELECT statements prior to a final list statements
#According to the OI reference manual.
$Q = @(
'SELECT CUSTOMERS WITH PHONE NE ""',
'SELECT CUSTOMERS WITH LNAME STARTING WITH "C"',
'LIST CUSTOMERS FNAME LNAME PHONE'
);

#Use powershell to join the criteria into a single string
$Qs = $Q -join ';';

#The parameters for the SELECT_INTO function.
$Args = @(
    $Qs,
    'TAB'
);

#Run the SELECT_INTO function and hold the result in $Result
$Result = $Server.CallFunction('SELECT_INTO',[ref] $Args);

#Convert the results lines into an array, one line per array element
$Lines = $Result -split "\n";

#Create a variable to hold the parsed results
$Table = @();

$LineNum = 0; #Count how many lines we've parsed.

ForEach ($Line in $Lines) {

    #Take one line and split it into fields using TABs
    $Line = $Line -split "\t";

    #Because we're taking TAB delimited data some of the lines might
    #Have white spare or new line characters, trim them off.
    $Line = $Line | foreach{ $_.Trim()};
    
    #Create an object to hold the line
    $O = New-Object PsCustomObject;

    If ($LineNum -eq 0) {
        #The first line of the output contains the field names
        $Fields = $Line;
       
    } else {
        #All other lines besides the first are data lines

        #Loop through each field and add a name value pair to the
        #Object which will represent the output.
        for($i = 0; $i -lt $Line.Count;$i++) {
           $O | Add-Member NoteProperty $Fields[$i] $Line[$i];
        }

        #Add the line object to the result table
        $Table += $o;

    }

    $LineNum += 1;
}

#Done
$Server.OIDisconnect();

Write-Host "Number of records returned $($Table.Count)";

#Show the table
$Table;