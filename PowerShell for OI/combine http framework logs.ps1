$LogPath = "E:\revsoft_ncb\HTTPLogs";

$OutputFilePath = "E:\revsoft_ncb\temp\combined.log";

$MatchingLogFiles = Get-ChildItem -Filter "*Request.log" -Path $LogPath;


foreach($LogFile in $MatchingLogFiles) {

    $Request = $LogFile.Name;
    
    $RequestFilePath = $LogFile.FullName;

    #Get the serial number from the file name
    $RequestSerial = $Request.Split('_')[3];

    #Remove the leading zeros from the serial number
    $ResponseSerial = $RequestSerial.TrimStart("0");

    #The response serial is one more is the response
    try {
        $ResponseSerial = [int]$ResponseSerial + 1;
    } catch {
        Write-Host "Unable to computer serial $RequestSerial from $Response";
        $RequestSerial = 0;
    }

    #Convert the response serial back to a padded string
    $ResponseSerial = $ResponseSerial.ToString();
    $ResponseSerial = $ResponseSerial.PadLeft(6,"0");

    #Generate the response filename
    $LogFilePath = $LogFile.FullName;
    $ResponseFilePath = $LogFilePath.Replace($RequestSerial + "_Request.log", $ResponseSerial + "_Response.log");

    Add-Content $OutputFilePath $($RequestFilepath + "`n");

    $Content = Get-Content $RequestFilePath;
    Add-Content $OutputFilePath $Content;

    Add-Content $OutputFilePath $($ResponseFilePath + "`n");

    $Content = Get-Content $ResponseFilePath;
    Add-Content $OutputFilePath $Content;
    

}