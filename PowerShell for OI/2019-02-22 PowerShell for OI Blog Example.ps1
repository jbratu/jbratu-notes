#Path to the .NET DLL containing NetOI installed by OpenInsight's clientsetup.exe
$DLLPath = "C:\Program Files (x86)\Revelation Software\Revelation Software .NET 4.0 Components v9.4.0\RevelationDotNet4.dll";

#
$OEHost = "localhost";
$Port = 8088;
$OIApp = "EXAMPLES";
$OIUser = "EXAMPLES";
$OIPass = "";

$Server = New-Object "Revelation4.NetOI.Server"

$Server.OIConnect($OEHost, $Port, $OIApp, $OIUser, $OIPass);

$Q = @(
'SELECT CUSTOMERS WITH PHONE NE ""',
'SELECT CUSTOMERS WITH LNAME STARTING WITH "C"',
'LIST CUSTOMERS FNAME LNAME PHONE'
);

$Qs = $Q -join ';';

$Args = @($Qs,'TAB');
$Result = $Server.CallFunction('SELECT_INTO',[ref] $Args);


$Lines = $Result -split "\n";
$Table = @();

$LineNum = 0;
ForEach ($Line in $Lines) {
    $Line = $Line -split "\t";

    $Line = $Line | foreach{ $_.Trim()};

    $O = New-Object PsCustomObject;

    If ($LineNum -eq 0) {
   
        $Fields = $Line;
       
    } else {

        for($i = 0; $i -lt $Line.Count;$i++) {
           $O | Add-Member NoteProperty $Fields[$i] $Line[$i];
        }

        $Table += $o;

    }

    $LineNum += 1;
}

$Server.OIDisconnect();

Write-Host "Number of records returned $($Table.Count)";