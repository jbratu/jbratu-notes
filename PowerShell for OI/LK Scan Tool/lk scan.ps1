$ScanPath = "C:\RevSoft\OI943";

$OEHost = "localhost";
$OEPort = 8088;
$OIApp = "SYSPROG";
$OIUser = "SYSPROG";
$OIPass = "";

#Path to the .NET DLL containing NetOI installed by OpenInsight's clientsetup.exe
$DLLPath = "C:\Program Files (x86)\Revelation Software\Revelation Software .NET 4.0 Components v9.4.0\RevelationDotNet4.dll";
#$DLLPath = "C:\Program Files (x86)\Revelation Software\Revelation Software .NET Components v9.5\RevelationDotNet.dll";

Try {
    $OITypes = Add-Type -Path $DLLPath -ErrorAction Stop;
} Catch {
    Write-Error "Unable to load $DLLPath";
    Break;
}
#Connection information for your script to access the OEngine service.

#Create a connection object
#$Server = New-Object "Revelation4.NetOI.Server"
$Server = New-Object -TypeName "Revelation.NetOI.Server"

$Utility = New-Object -TypeName "Revelation.NetOI.Utility"

$Server.StartupFlags = 1

#Connect to the server
$Server.OIConnect($OEHost, $OEPort, $OIApp, $OIUser, $OIPass);

#$p3 = 'TABLE_NAME' + $Server.ourRM + 'TABLE_FOREIGN_NAME';
#$p3 = 'TABLE_NAME' + $Server.ourFM + 'TABLE_FOREIGN_NAME';
#$p3 = 'TABLE_NAME' + [char]254 + 'TABLE_FOREIGN_NAME';

#This passes through as a VM
#$p3 = 'TABLE_NAME' + [char]253 + 'TABLE_FOREIGN_NAME';



$Routine = 'LIST_VOLUME_SUB';
$Routine = 'TEMP_JAB';


#According to the OI help file SELECT_INTO has two parameters.
#We will pass those parameters as an array.
$Args = @(
'LIST_VOLUME_SUB',
'DATAVOL',
'',
('TABLE_NAME' + '|@FM' + 'TABLE_FOREIGN_NAME'),
''
);

#Use the connection to execute SELECT_INTO with our parameters.
$Result = $Server.CallFunction("CS_PS_WRAPPER",[ref] $Args);

$Server.Call
#Display the results and disconnect

$Tables = $Result.Split("`r`n");

$Server.OIDisconnect();



for($i = 0; $i -le $Tables.Count ; $i++) {

    $Table = $Tables[$i];

    If($Table -ne $null) {

        $Table = $Table.Split("�");

        $TableObj = [pscustomobject]@{
                        table = $Table[0]
                        file = $Table[1]
                    }

        $Tables[$i] = $TableObj;

    }
}

#Write-Host $Tables;
$Tables | Format-Table

return

$Files = Get-ChildItem -Path ($ScanPath + "\*") -Include *.lk

ForEach ($LKFile in $Files) {

    $OVFullName = $LKFile.FullName.Replace(".LK",".OV");

    try {
        $OVFile = Get-ChildItem $OVFullName -ErrorAction Stop

        $LKSize = $LKFile.Length;
        $OVSize = $OVFile.Length;

        if ($OVSize -gt 0) {

            $Ratio = $LKSize / $OVSize
            $Ratio = [math]::Round($Ratio, 1);

            if ($Ratio -gt 1) {
                # This file is suspect for resize

                Write-Host "$($LKFile.Name) ratio $Ratio";

            }

        } else {
            #This file is not active, OV size is 0 or doesn't exist
        }

    } catch {
        Write-Warning "File $OVFullName does not exist";
    }

    
}

