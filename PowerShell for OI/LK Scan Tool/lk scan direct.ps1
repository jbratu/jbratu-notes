#
# Description:
# Given a OI folder find all the LK files and then locate the corresponding OV files and
# generate a list of the ratio based on file sizes.
#

$ScanPath = "C:\Revsoft\OInsight94";

#If greater than 0 only tables which are larger than the size specified will be considered in the output.
$MinimumTableSizeInMB = 0;

$Files = Get-ChildItem -Path ($ScanPath + "\*") -Include *.lk -Recurse

$Results = @();

ForEach ($LKFile in $Files) {

    $OVFullName = $LKFile.FullName.Replace(".LK",".OV");

    try {
        $OVFile = Get-ChildItem $OVFullName -ErrorAction Stop

        $LKSize = $LKFile.Length;
        $OVSize = $OVFile.Length;

        $SkipFile = $False;

        If($MinimumTableSizeInMB -gt 0) {
            # Files must be larger than the size specified to be eligible.
            
            # Calculate the size of the LK and OV in MB
            $TableSize = $LKSize + $OVSize;
            $TableSize = $TableSize / 1024 / 1024;

            if ($TableSize -le $MinimumTableSizeInMB) {
                # This table does not meet the size requirements. Skip it.
                $SkipFile = $true;
            }

        } else {
            # No minimum. Do not skip this file.
            $SkipFile = $False;
        }

        if ($OVSize -gt 1 -and $SkipFile -eq $false) {

            $Ratio = ($OVSize * 100 / $LKSize) / 100
            $Ratio = [math]::Round($Ratio, 1);

            if ($Ratio -gt 1) {
                # This file is suspect for resize

                #Write-Host "$($LKFile.FullName) ratio $Ratio";

                $ourObject = New-Object -TypeName psobject 
                $ourObject | Add-Member -MemberType NoteProperty -Name File -Value $LKFile.FullName
                $ourObject | Add-Member -MemberType NoteProperty -Name Ratio -Value $Ratio

                $ourObject | Add-Member -MemberType NoteProperty -Name LKSize -Value ([math]::Round(($LKSize / 1MB)))
                $ourObject | Add-Member -MemberType NoteProperty -Name OVSize -Value ([math]::Round(($OVSize / 1MB)))

                $Results += $ourObject;


            }

        } else {
            #This file is not active, OV size is 0 or doesn't exist
        }

    } catch {
        Write-Warning "File $OVFullName does not exist";
    }

    
}

$Results | Sort-Object -Descending Ratio | Format-Table

