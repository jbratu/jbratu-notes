#Search the REST API HTTP Logs for either a request or response that contains the search
#text and show both files regardless of which contains the search term


#What directory do we want to look for log files in?
$LogPath = "C:\HTTPLogs"

#What search pattern to use?
$SearchPattern = "Mon, 19 Oct 2020 04:20:30 GMT"

#
#Find all the log files with the search pattern recusively after the date indicated
#$MatchingLogFiles = Get-ChildItem "*.log" -Path $LogPath -Recurse |
#    Where-Object { $_.LastWriteTime -ge "09/15/2020"} |
#    Select-String -Pattern $SearchPattern 

#
#Find all the log files with the search pattern recusively between the date range indicated
#$MatchingLogFiles = Get-ChildItem "*.log" -Path $LogPath -Recurse |
#    Where-Object { $_.LastWriteTime -ge "09/27/2013" -and $_.LastWriteTime -le "09/28/2020" } |
#    Select-String -Pattern $SearchPattern 
    
#File all the log files with the search patterh recursively - any date
$MatchingLogFiles = Get-ChildItem "*.log" -Path $LogPath -Recurse |
    Select-String -Pattern $SearchPattern 

#Loop through all the matches and try to find the file pairs of the request
foreach($LogFile in $MatchingLogFiles) {
    
    $MatchFileName = $LogFile.FileName;
    Write-Host "Found Match $MatchFileName";

    #Assume file is neither a request or response
    $Request = '';
    $Response = '';
    $RequestFilePath = '';
    $ResponseFilePath = '';

    #Is this a request or a response log?
    if ($MatchFileName -match '^.+_Response\.log$') {
        #This is a Response so we need to generate the request filename
        $Response = $LogFile.FileName;
        $ResponseFilePath = $LogFile.Path;

        #Get the serial number from the file name
        $ResponseSerial = $Response.Split('_')[3];

        #Remove the leading zeros from the serial number
        $RequestSerial = $ResponseSerial.TrimStart("0");

        #The request serial is one less than response
        Try {
            $RequestSerial = [int]$RequestSerial - 1;
        } catch {
            Write-Host "Unable to computer serial $RequestSerial from $Response";
            $RequestSerial = 0;
        }

        #Convert the response serial back to a padded string
        $RequestSerial = $RequestSerial.ToString();
        $RequestSerial = $RequestSerial.PadLeft(6,"0");

        #Generate the response filename
        $LogFilePath = $LogFile.Path
        $RequestFilePath = $LogFilePath.Replace($ResponseSerial + "_Response.log", $RequestSerial + "_Request.log");

    } elseif ($MatchFileName -match '^.+_Request\.log$') {
        #This is a Request so we need to generate the response filename
        $Request = $LogFile.FileName;
        $RequestFilePath = $LogFile.Path;

        #Get the serial number from the file name
        $RequestSerial = $Request.Split('_')[3];

        #Remove the leading zeros from the serial number
        $ResponseSerial = $RequestSerial.TrimStart("0");

        #The response serial is one more is the response
        try {
            $ResponseSerial = [int]$ResponseSerial + 1;
        } catch {
            Write-Host "Unable to computer serial $RequestSerial from $Response";
            $RequestSerial = 0;
        }

        #Convert the response serial back to a padded string
        $ResponseSerial = $ResponseSerial.ToString();
        $ResponseSerial = $ResponseSerial.PadLeft(6,"0");

        #Generate the response filename
        $LogFilePath = $LogFile.Path;
        $ResponseFilePath = $LogFilePath.Replace($RequestSerial + "_Request.log", $ResponseSerial + "_Response.log");

    }

    if ($RequestFilePath -eq '' -or $ResponseFilePath -eq '') {
        #No matching filename pair was found, skip
        Write-Host "Skipping invalid filename $MatchFileName";
    } else {
		#We have a matching file pair so show the request and response log content
		
        Write-Host "#####################################################################################";
        Write-Host "Found match $RequestFilePath $ResponseFilePath";
        Write-Host "#####################################################################################";

        Write-Host "`n`n $($RequestFilePath):";
        Get-Content -Path $RequestFilePath;
        Write-Host "`n`n $($ResponseFilePath):";
        Get-Content -Path $ResponseFilePath;
    }
    
}
