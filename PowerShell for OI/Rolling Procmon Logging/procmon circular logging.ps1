﻿#
# This script will run for 1 day restarting process monitor every 10 minutes and keeping 75 minutes of old logs
#

$LogPath = “C:\Temp\ProcMon”
$Counter = 0
$MaxLogs = 24 * 12
do{
   $Logfile = $LogPath+”\Logfile-$Counter-Circular.pml”
  $ProcMonParameters = "/Backingfile $Logfile /AcceptEula /Minimized /Quiet"



  # Start ProcMon for 5 minutes
  start-process $LogPath\procmon.exe $ProcMonParameters
  Start-Sleep -Seconds 600

  # Terminate ProcMon
  C:\Temp\ProcMon\procmon.exe /Terminate
  Start-Sleep 3

  # Compress log files and remove trace files

  $CurrentDate = Get-Date
  $DatetoDelete = $CurrentDate.AddMinutes(-75);
  Get-ChildItem $LogPath -Recurse  -Include *.pml | Where-Object { $_.LastWriteTime -lt $DatetoDelete } | Remove-Item;
  
  
  #Remove-Item $Logpath\*.pml

  $Counter += 1;

} while ($Counter -le $MaxLogs)