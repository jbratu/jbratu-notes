﻿#
# The purpose of this is to be run from the task scheduler every 15 minutes
# The script will run for $RunTime seconds and purge any old log files older than 1440 minutes (one day)
#

$AppPath = "D:\Procmon Continous Periodic Logging"
$LogPath = “D:\Procmon Continous Periodic Logging\Logs”
$Counter = 0
$RunTime = 300

$Logfile = $($LogPath+”\Logfile-Circular $((get-date).tostring('MMddyyyyHHmmss')).pml”)

$ProcMonParameters = "/Backingfile ""$Logfile"" /AcceptEula /Minimized /Quiet";


$AppPathExe = "`"$AppPath\procmon.exe`"";
Write-Host $AppPathExe;
Write-Host $ProcMonParameters;

start-process $AppPathExe $ProcMonParameters
Start-Sleep -Seconds $RunTime

# Terminate ProcMon
start-process $AppPathExe /Terminate
Start-Sleep 3

# Compress log files and remove trace files

$CurrentDate = Get-Date

# Keep a days work of logs
$DatetoDelete = $CurrentDate.AddMinutes(-1440);
Get-ChildItem $($LogPath + "logs") -Recurse  -Include *.pml | Where-Object { $_.LastWriteTime -lt $DatetoDelete } | Remove-Item;
  
  
#Remove-Item $Logpath\*.pml

$Counter += 1;
