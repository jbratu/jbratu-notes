#What client permission will be set?
$RESTRICTED_ACCOUNTS = @("BuffaloOlmstead");

#Where is this client's home directory?
$FolderPath = "C:\BGBase\Buffalo_olmstead";

#Who are the admin users
$ADMIN_ACCOUNTS = @("bgbase\srpadmin", "bgbase\bbgadmin", "domain admins","SYSTEM");


   
Write-host "Working on $FolderPath"

#Reset the permissions to ensure we don't miss anything
icacls.exe "$FolderPath" /reset /T

#Build a list of all the grant accounts   
foreach ($Account in $ADMIN_ACCOUNTS) {
        $permArgs += "/grant:r ""$Account"":(OI)(CI)(F) /T ";
    }

$args = @"
"$FolderPath" $permArgs
"@;

Start-Process icacls.exe -ArgumentList @($args) -Wait;

#Remove inheriteance from parent
    $args = @"
"$FolderPath" /inheritance:r
"@;
    Start-Process icacls.exe -ArgumentList @($args) -Wait;

#Remove and verify restricted accounts has no rights except what we give
    foreach ($Account in $RESTRICTED_ACCOUNTS) {
        $args = @"
"$FolderPath" /Remove "$Account" /T
"@;
        Start-Process icacls.exe -ArgumentList @($args) -Wait;
    }


#Apply RX rights for the restricted user
    foreach ($Account in $RESTRICTED_ACCOUNTS) {
        $args = @"
"$FolderPath" /grant:r "$Account":(RX) /T
"@;
        Start-Process icacls.exe -ArgumentList @($args) -Wait;
    }

    #Allow restricted user to list but not read files
    foreach ($Account in $RESTRICTED_ACCOUNTS) {

        #Secure the LK Files
        $args = @"
"$FolderPath\*.LK" /grant:r "$Account":(X) /T
"@;
        Start-Process icacls.exe -ArgumentList @($args) -Wait;

        #Secure the OV Files
        $args = @"
"$FolderPath\*.OV" /grant:r "$Account":(X) /T
"@;
        Start-Process icacls.exe -ArgumentList @($args) -Wait;
    }

