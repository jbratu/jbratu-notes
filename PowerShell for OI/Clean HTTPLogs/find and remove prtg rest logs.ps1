﻿#What directory do we want to look for log files in?
$LogPath = "\\ncb-testweb\PROGRAM\revsoft_ncb\HTTPLogs"

#Find all the request log files from PRTG
$MatchingLogFiles = Get-ChildItem "*_Request.log" -Path $LogPath | Select-String -Pattern "PRTG Network Monitor" 

foreach($LogFile in $MatchingLogFiles) {
    $Request = $LogFile.Path;

    #Get the serial number from the file name
    $RequestSerial = $Request.Split('_')[4];

    #Remove the leading zeros from the serial number
    $ResponseSerial = $RequestSerial.TrimStart("0");

    #The response serial is one more is the response
    $ResponseSerial = [int]$ResponseSerial + 1;

    #Convert the response serial back to a padded string
    $ResponseSerial = $ResponseSerial.ToString();
    $ResponseSerial = $ResponseSerial.PadLeft(6,"0");

    #Generate the response filename
    $Response = $Request.Replace($RequestSerial + "_Request.log", $ResponseSerial + "_Response.log");

    #Remove the request and response files
    Remove-Item -Path $Request;
    Remove-Item -Path $Response;
    Write-Host "Removing $Request $Response";
}

#| Select-Object -ExcludeProperty path | Write-Host "Found $_#| Remove-Item -Force -Confirm