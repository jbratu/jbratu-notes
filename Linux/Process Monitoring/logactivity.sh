#!/bin/bash
/usr/bin/free -m >> /var/log/apache2/fullstatus.log
/usr/sbin/apachectl fullstatus >> /var/log/apache2/fullstatus.log
/usr/bin/mysql -u root -p{{PW}} -e "SHOW PROCESSLIST" >> /var/log/apache2/fullstatus.log

#
# Schedule this to run from CRON
# * * * * * /root/scripts/logactivity.sh
#
