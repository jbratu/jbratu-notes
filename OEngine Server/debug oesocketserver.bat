REM Don't forget to stop the OEngineServer Service and start it when finished.

REM Kill any previous sessions
taskkill /fi "WINDOWTITLE eq Administrator:  OEngineServer"

REM Delay a few seconds for the kill to finish
ping localhost

REM Set the title of this window so we can find it. This should be unique.
TITLE OEngineServer

REM
REM Get the date and time
REM 
For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set mydate=%%c-%%a-%%b)
For /f "tokens=1-2 delims=/:" %%a in ('time /t') do (set mytime=%%a%%b)

CD /D C:\Revsoft\OpenInsight

#Launch java with memory limits, a custom config, and log to a file.
#Java -Xmx1512m -Xms1512m -jar OESocketServer.jar -l eserver-test.cfg -d 6 > "oesocketserver_debug_test_%mydate%_%mytime%.txt"
#Java -Xmx1512m -Xms1512m -jar OESocketServer.jar -l eserver-test.cfg -d 6
Java -jar OESocketServer.jar -d 3

REM Use this if you need to see why the console closed
REM pause