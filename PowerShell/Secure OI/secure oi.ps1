﻿

#Location of OI
$OIPath = "C:\Revsoft\Oinsight";

#Users who have admin access
$ADMIN_ACCOUNTS = @("SYSTEM", "domain\user", "Domain Admins");

#Agency users - accounts with ability to change certain things
$AGENCY_USERS = @{}

#Users who have limited access and not access to LK/OV files
$RESTRICTED_ACCOUNTS = @("Everyone");


if(![System.IO.Directory]::Exists($OIPath)){
    Write-Error "The OpenInsight directory $OIPath does not exist";
    Return
}

#Optional list of sub-folders to exclude from process
#ICACLS doesn't allow excluding directories so build a list of top level folders to scan
$ExcludeFolders = @();

#For testing, only selected folders
$ChangeFolders = Get-ChildItem | ?{ $_.PSIsContainer } | Select-Object Name | ForEach {"$($_.Name)"}

#
# Set the root client folder permissions
#

$FolderPath = $OIPath;
Write-host "Working on $FolderPath"

#
#Reset the permissions
#
$args = @"
"$FolderPath" /reset /T
"@;
Write-Host "icacls.exe -ArgumentList $args";
Start-Process icacls.exe -ArgumentList $args -Wait;


#
# Grant admin rights
#
foreach ($Account in $ADMIN_ACCOUNTS) {
    $args = @"
    "$FolderPath" /grant:r "$Account":(OI)(CI)(F) /T
"@;

    Write-Host "icacls.exe $args";

    Start-Process icacls.exe -ArgumentList $args -Wait;
}

#
#Remove inheriteance from parent
#
$args = @"
"$FolderPath" /inheritance:r
"@;
Write-Host "icacls.exe $args";
Start-Process icacls.exe -ArgumentList $args -Wait;

 
#Allow agency users to have full control over their own folder     
#foreach ($Account in $AGENCY_USERS) {
#    $args = @"
#    "$FolderPath" /grant:r "$Account":(OI)(CI)(M) /T
#"@;
#
#    Start-Process icacls.exe -ArgumentList $args -Wait;
#}

#
# Grant restricted rights
#
foreach ($Account in $RESTRICTED_ACCOUNTS) {

    #Grant read and execute for entire OI folder
    $args = @"
    "$FolderPath" /grant:r "$Account":(RX) /T
"@;

    Write-Host "icacls.exe $args";
    Start-Process icacls.exe -ArgumentList $args -Wait;

    #Prevent read on LK files
    $args = @"
    "$FolderPath\*.LK" /grant:r "$Account":(X) /T
"@;

    Write-Host "icacls.exe $args";
    Start-Process icacls.exe -ArgumentList $args -Wait;

    #Prevent read on OV files
    $args = @"
    "$FolderPath\*.OV" /grant:r "$Account":(X) /T
"@;

    Write-Host "icacls.exe $args";
    Start-Process icacls.exe -ArgumentList $args -Wait;
}

return