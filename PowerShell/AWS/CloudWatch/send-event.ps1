﻿# Install with Install-Module -Name AWSPowerShell
$count = 15;
Import-Module AWSPowerShell
# AWS Defaults data
#$param = @{
#    region      = 'ap-south-1';
#    AccessKey   = 'REDACTED';
#    SecretKey   = 'REDACTED';
#}

# Initialize AWS
$MyProfileName = 'jab-admin';
#Set-AWSCredential -AccessKey 'REDACTED' -SecretKey 'REDACTED' -StoreAs 'jab-admin'

$param = @{
    region      = 'ap-south-1';
}

#-ProfileName MyProfileName
Initialize-AWSDefaults @param -Verbose  -ProfileName $MyProfileName;
# Instance and namespace variables
$namespace = "Usage Metrics"
#$instanceID = ""
$dat = New-Object Amazon.CloudWatch.Model.MetricDatum
$dat.Timestamp = (Get-Date).ToUniversalTime()
$dat.MetricName = "New Claims MetricDatum Test"
$dat.Unit = "Count"
$dat.Value = $count
#Get-EC2Instance -ProfileName MyProfileName
Write-CWMetricData -Namespace $namespace -MetricData $dat