$usernameToCheck = 'tcooper';
$currentDate = Get-Date -Format "yyyy-MM-dd";
$logfilename = "$PSScriptRoot\logs\$currentDate disconnect dev log.txt";
Start-Transcript -Path $LogFileName -NoClobber -Append;

$quserResult = quser 2>&1;

$userPresent = $false;
$userIdleTooLong = $false;
$adminPresent = $False;

#write-host $quserResult;
	If ( $quserResult.Count -gt 0 )
{
		$quserRegex = $quserResult | ForEach-Object -Process { $_ -replace '\s{2,}',',' }
		#write-host $quserRegex;
		$quserObject = $quserRegex | ConvertFrom-Csv
		#write-host $quserObject;
		
		foreach ($userObj in $quserObject) {
			#write-host $userObj;
			#write-host $userObj.USERNAME;
			
			if($userObj.USERNAME -eq $usernameToCheck) {
				$userPresent = $True;
				
				if($userObj.STATE -ne 'Active') {
					# State will contain the idle time
					
					
					$idleTime = $userObj.STATE;
					
					if($idleTIme -eq '.') {
						$idleTime = 0;
					}
					
					$timeParts = $idleTime -Split ':';
					if($timeParts.length -eq 1) {
						# We only have minutes
						$idleTimeMinutes = [int]$timeParts[0];
					} else {
						#We must have hours and minutes
						$idleTimeMinutes = ([int]$timeParts[0] * 60) + [int]$timeParts[1];
					}
					
					Write-Host "Time idle is $idleTime which is $idleTimeMinutes minutes";
					
					if($idleTimeMinutes -gt 90) {
						Write-Host 'Time to shutdown';
						$userIdleTooLong = $True;
					} else {
						Write-Host "User idle for $idleTimeMinutes minutes";
						$userIdleTooLong = $False;
					}
					
				}
				
			}
			
			if($userObj.USERNAME -eq 'administrator' -or $userObj.USERNAME -eq '>administrator') {
				$adminPresent = $True;
			}
			
		}
		
		#$userSession = $quserObject | Where-Object -FilterScript { $_.USERNAME -eq 'tcooper' }
		#If ( $userSession )
		#{
	#		logoff $userSession.ID /server:$computer
	#	}
	}

if($adminPresent -eq $true) {
	Write-Host 'Skipping check because admin logged in';
	exit;
}
	
If($userPresent -eq $false) {
	Write-Host 'User Not logged in. Shuttind down';
	Stop-Computer -Force;
} else{
	Write-Host 'Checking if user idle';
	
	If ($userIdleTooLong -eq $True) {
		Write-Host 'Time to shutdown';
		Stop-Computer -Force;
	} else {
		Write-Host 'User Active';
	}
}