$DesktopPath = [Environment]::GetFolderPath("Desktop")
$Msg = "DEV COPY"

$filename = "$DesktopPath\devcopy.bmp" 
$bmp = new-object System.Drawing.Bitmap 250,61 
$font = new-object System.Drawing.Font Consolas,24 
$brushBg = [System.Drawing.Brushes]::LightGray
$brushFg = [System.Drawing.Brushes]::Black 
$graphics = [System.Drawing.Graphics]::FromImage($bmp) 
$graphics.FillRectangle($brushBg,0,0,$bmp.Width,$bmp.Height) 
$graphics.DrawString($Msg,$font,$brushFg,10,10) 
$graphics.Dispose() 
$bmp.Save($filename) 

Invoke-Item $filename  