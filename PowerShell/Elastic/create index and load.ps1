﻿$Recs = Import-CSV -Delimiter '|' -Path "C:\temp\csdatasample_1.txt";
#$Recs = Import-CSV -Delimiter '|' -Path "C:\temp\test.txt";

$APIHost = 'http://192.168.11.21:9200';

$URI = $APIHost + '/csdatasample';

$header = @{
  "Content-Type"="application/json; charset=utf-8"
} 

$body = @"
{
  "settings": {
    "number_of_shards": 4
  },
  "mappings": {
    "properties": {
      "ID": { "type": "long" },
      "FName": { "type": "text" },
      "LName": { "type": "text" },
      "City": { "type": "text" },
      "Zip": { "type": "text" },
      "Country": { "type": "text" }

    }
  }
}
"@;

#Remove the existing index
try {
    Invoke-RestMethod -Method DELETE -Uri $URI -Headers $header;
} catch {
    $_.ErrorDetails.Message | ConvertFrom-Json | ConvertTo-Json 
}

#Create a new index with mapping
$URI = $APIHost + '/csdatasample/contact';
try {
    Invoke-RestMethod -Method POST -Uri $URI -Headers $header -Body $body;
} catch {
    $_.ErrorDetails.Message | ConvertFrom-Json | ConvertTo-Json 
}

#Load the records into the index
$URI = $APIHost + '/csdatasample/contact';

ForEach ($rec in $Recs) {

    write-host $rec;

    #$ID = [long]$rec.ID;

    #$rec.PSObject.properties.remove('ID');
    #Add-Member -InputObject $rec -NotePropertyName 'ID' -NotePropertyValue $ID;

    $o = New-Object -TypeName psobject
    Add-Member -InputObject $o -NotePropertyName 'ID' -NotePropertyValue ([long]($rec.ID));
    Add-Member -InputObject $o -NotePropertyName 'FName' -NotePropertyValue $rec.FName;
    Add-Member -InputObject $o -NotePropertyName 'LName' -NotePropertyValue $rec.LName;
    Add-Member -InputObject $o -NotePropertyName 'City' -NotePropertyValue $rec.City;
    Add-Member -InputObject $o -NotePropertyName 'Zip' -NotePropertyValue $rec.Zip;
    Add-Member -InputObject $o -NotePropertyName 'Country' -NotePropertyValue $rec.Country;

    $oJson = $o | ConvertTo-JSON;
    $oJson = [System.Text.Encoding]::UTF8.GetBytes($oJson)

    try {
        Invoke-RestMethod -Method POST -Uri $URI -Headers $header -Body $oJson;
    } catch {
        $_.ErrorDetails.Message | ConvertFrom-Json | ConvertTo-Json 
    }

}