﻿$Recs = Import-CSV -Delimiter '|' -Path "C:\temp\test.txt";

$APIHost = 'http://192.168.11.21:9200';

$URI = $APIHost + '/csdatasample';

$header = @{
  "Content-Type"="application/json; charset=utf-8"
} 

#Load the records into the index
$URI = $APIHost + '/csdatasample/contact/_bulk';

$upload = "";

ForEach ($rec in $Recs) {

    #write-host $rec;

    #$ID = [long]$rec.ID;

    #$rec.PSObject.properties.remove('ID');
    #Add-Member -InputObject $rec -NotePropertyName 'ID' -NotePropertyValue $ID;

    $o = New-Object -TypeName psobject
    Add-Member -InputObject $o -NotePropertyName 'ID' -NotePropertyValue ([long]($rec.ID));
    Add-Member -InputObject $o -NotePropertyName 'FName' -NotePropertyValue $rec.FName;
    Add-Member -InputObject $o -NotePropertyName 'LName' -NotePropertyValue $rec.LName;
    Add-Member -InputObject $o -NotePropertyName 'City' -NotePropertyValue $rec.City;
    Add-Member -InputObject $o -NotePropertyName 'Zip' -NotePropertyValue $rec.Zip;
    Add-Member -InputObject $o -NotePropertyName 'Country' -NotePropertyValue $rec.Country;

    $oJson = $o | ConvertTo-JSON;
    $oJson = $oJson.Replace("`r","");
    $oJson = $oJson.Replace("`n","");

    $oJson = "{ `"index`":{} }`r`n" + $oJson + "`r`n";


    $payload += $oJson;
    
}

$payload = [System.Text.Encoding]::UTF8.GetBytes($payload)



try {
    Invoke-RestMethod -Method POST -Uri $URI -Headers $header -Body $payload;
} catch {
    $_.ErrorDetails.Message | ConvertFrom-Json | ConvertTo-Json 
}



#write-host $payload;