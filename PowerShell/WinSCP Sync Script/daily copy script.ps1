﻿#Username for server
$Username = "cpebackup";

#Server name to connect to
$HostName = "cpeonline.com";

#SHA fingerprint for server
$Fingerprint = "ecdsa-sha2-nistp256 256 ngUjRgNxIWN4S0cRysnZca2jpU0I7RcQDGNR1x33fOs=";

#Where should the backups be stored?
$BaseDir = [Environment]::GetFolderPath("MyDocuments") + "\backups " + $hostname

#What do we want to backup
$LocalPath = "$BaseDir\daily\";
$RemotePath = "daily/cpelive";

$TarsLocalPath = "$BaseDir\tars\";
$TarsRemotePath = "tars";

#Create the backup store if it doesn't exist
If([System.IO.Directory]::Exists($LocalPath) -eq $False) {
    New-Item $LocalPath -ItemType Directory | Out-Null;
}

If([System.IO.Directory]::Exists($TarsLocalPath) -eq $False) {
    New-Item $TarsLocalPath -ItemType Directory | Out-Null;
}

#Location of the stored encrypted credentials
$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
$CredStoreFile = $($ScriptDir + "\copycreds.bin");

If([System.IO.File]::Exists($CredStoreFile) -eq $False) {
    #The credentials don't exist so prompt for them and store them.
	read-host "Please enter the password for $Username on $HostName" -assecurestring | ConvertFrom-SecureString | out-file $CredStoreFile;
}

Write-Host "The backup is running";

#Read the credentials back and convert to string for use
$SecurePassword = get-content $CredStoreFile | ConvertTo-SecureString;
$BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
$Password = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)

#Set some WinSCP properties
New-ItemProperty -Path "HKCU:\SOFTWARE\Martin Prikryl\WinSCP 2\" -Name DefaultUpdatesPeriod -Value 0 -ErrorAction SilentlyContinue -PropertyType DWord;
Set-ItemProperty -Path "HKCU:\SOFTWARE\Martin Prikryl\WinSCP 2\" -Name DefaultUpdatesPeriod -Value 0;

try
{
    # Load WinSCP .NET assembly
    Add-Type -Path (Join-Path $PSScriptRoot "WinSCPnet.dll")
 
    # Setup session options
    $sessionOptions = New-Object WinSCP.SessionOptions -Property @{
        Protocol = [WinSCP.Protocol]::Sftp
        HostName = $Hostname
        UserName = $UserName
        Password = $Password
        SshHostKeyFingerprint = $Fingerprint
    }
 
    $session = New-Object WinSCP.Session
 
    try
    {
        # Connect
        $session.Open($sessionOptions)
 
        # Upload files
        $transferOptions = New-Object WinSCP.TransferOptions
        $transferOptions.TransferMode = [WinSCP.TransferMode]::Binary

        $session.SynchronizeDirectories([WinSCP.SynchronizationMode]::Local,$LocalPath,$RemotePath, $True,$True);

        $session.SynchronizeDirectories([WinSCP.SynchronizationMode]::Local,$TarsLocalPath,$TarsRemotePath, $True,$True);
    }
    finally
    {
        # Disconnect, clean up
        $session.Dispose()
    }
 
    Write-Host "Backup completed"
    exit 0
}
catch
{
    Write-Host "Error: $($_.Exception.Message)"
    exit 1
}
