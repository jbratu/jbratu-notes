﻿# CPEOnline.com to Office Backup Script

Revision 1.

## Configure Environment for PowerShell scripts
Prior to using scripts please follow these steps once to configure PowerShell scripts.

1. Launch powershell.exe 'as administrator'

2. Execute the command:  
  
`set-executionpolicy unrestricted`

3. Enter 'y' when prompted

![](assets/2019-09-09_9-16-36.png)

Scripts are now ready to run on the system.

## Set the credentials
The first time the backup script runs it prompts for credentials and stores them in an encrypted file that only the user who ran the script can access.

1. Right click on the `daily copy script.ps1` and select 'Run With PowerShell'

2. Enter the password when prompted.

![](assets/2019-09-09_15-30-06.png)

3. The backup will run until completed. 

## Resetting the Credentials

Delete the `copycreds.bin` file and re-run the script to enter the credentials again.

## Run the Script As Needed

1. Right click on the `daily copy script.ps1` and select 'Run With PowerShell'

The script will connect to cpeonline.com and backup the files into your `My Documents\backups cpeonline.com`

## Run the Script As As a Scheduled Job

1. Create a windows scheduled task

2. Enter the parameters:  

![](assets/2019-09-09_15-37-08.png)

Program:

`powershell.exe`

Add arguments:

`-File "C:\Path\To\daily copy ps1"`

3. Schedule the script to run some time after 4 AM EST to allow enough time for the on server backup to run. The script can run during the day without impacting the site.

Check the `My Documents\backups cpeonline.com` to ensure the files are updating.



