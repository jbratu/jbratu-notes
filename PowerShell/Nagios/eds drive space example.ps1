$nagioshost = "edswww";
$service = "PortalFreeSpace";
$pw = "";
$un = "A271E0F344";


$FreeSpaceWarningThreshold = 30;
$FreeSpaceCriticalThreshold = 15;
$FreeSpaceMinimum = 50 * 1024 * 1024 * 1024;

New-PSDrive -Name DB -PSprovider FileSystem -root /mnt/db -ErrorAction SilentlyContinue
new-psdrive -name vhosts -PSProvider FileSystem -root /mnt/vhosts -ErrorAction SilentlyContinue
new-psdrive -name files -PSProvider FileSystem -root /mnt/sharedfiles -ErrorAction SilentlyContinue
new-psdrive -name root -PSProvider FileSystem -root / -ErrorAction SilentlyContinue

$CalculatedState = 0;
#Once an alert is triggered don't allow it to be canceled by a drive that has a lot of free space
$AlertTriggered = 0;

$drives = get-psdrive | Where-Object {$_.Provider.Name -eq 'FileSystem'}
foreach ($drive in $drives) {
    $size = $drive.Used + $drive.Free;
    $freePercentage = ($drive.Free * 100) / $size;
    $freePercentage = [math]::Round($freePercentage,1);
    Write-Host $drive.name is $freePercentage;

    if ($freePercentage -le $FreeSpaceCriticalThreshold) {
        $CalculatedState = 2;
        $msg = "Free space of $($drive.name) is less than $freePercentage percent";
    } elseif ($freePercentage -le $FreeSpaceWarningThreshold) {
        $CalculatedState = 1;
        $msg = "Free space of $($drive.name) is less than $freePercentage percent";
    }

    if ($CalculatedState -gt 0 -and $AlertTriggered -eq 0) {
        # Free space percentage triggers a warning state but don't consider it a problem
        # if there is enough free space.
        Write-Host $drive.name at warning level. Free $drive.free minimum $FreeSpaceMinimum

        if ($drive.free -ge $FreeSpaceMinimum) {
            #The drive is very large and there is enough free space
            $CalculatedState = 0;
            $msg = "CheckOK";

            Write-Host "Canceling alert";
        } else {
            #An alert must be sent for this drive.
            $AlertTriggered = 1;
        }
    
    }

}


$uri = "https://www.srpcs.com/cgi-bin/nagios3/cmd.cgi?cmd_typ=30&cmd_mod=2&host=$nagioshost&service=$service&plugin_state=$CalculatedState&plugin_output=$msg&btnSubmit=Commit";
Write-Host $uri;
$cred = New-Object System.Management.Automation.PSCredential -ArgumentList $un, (ConvertTo-SecureString $pw -AsPlainText -Force)

Invoke-WebRequest -Uri $uri -Credential $cred -OutFile $null
