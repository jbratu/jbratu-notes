For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set mydate=%%c-%%a-%%b)
For /f "tokens=1-2 delims=/:" %%a in ('time /t') do (set mytime=%%a%%b)

REM Capture 24 files and stop capturing
REM Store 300 seconds worth of captures in each file
REM This should be 2 Hours worth of captures
REM Capture TCP 1777
"C:\Program Files\Wireshark\Wireshark.exe" -i "Ethernet 2" -k -a files:24 -b duration:300 -f "tcp port 1777" -w "C:\TEMP\WireShark Captures\sfcada-%mydate%_%mytime%"